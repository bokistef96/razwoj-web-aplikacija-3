import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';

import { FETCH_STUDENTS,FetchStudentsSuccess } from '../actions';
import { StudentsService } from '../../service/student.service';

//- Actions -> observable 
@Injectable() 
export class StudentsEffects {

    constructor(
        private actions$: Actions,
        private studentsService: StudentsService    
    ) {}

    @Effect()
    loadBooks$ = this.actions$.ofType(FETCH_STUDENTS).pipe(
            switchMap(() => {  
              console.log("aaaa");
                return this.studentsService.fetchStudents().pipe(  
                    map(students => new FetchStudentsSuccess(students))
                )}
            )
    );  
                        
}                                                             