import { Action } from "@ngrx/store";
import { Student } from "../../models/student";
import { RETURN_STUDENTS, ADD_STUDENT, addStudent, DELETE_STUDENT, deleteStudent,STUDENTS_FETCH_DONE,FetchStudentsSuccess } from "../actions";
import { getStudents, addNewStudent, deleteStudentA } from "../../service/api";
import { createEntityAdapter, EntityState, EntityAdapter } from '@ngrx/entity';
import  { StudentsService }  from "../../service/student.service"
    
export interface StudentState extends EntityState<Student>{
    ids: number[],
    entities: { [id: number] : Student}
}

const adapter: EntityAdapter<Student> = createEntityAdapter<Student>();

const initalState: StudentState = {
    ids: [],
    entities: { }
}

export default function (state: StudentState = initalState, action: Action) {
    switch(action.type) {
        case ADD_STUDENT:{
            const {student} = action as addStudent;            
            return adapter.addOne(student, state);
        }
        case STUDENTS_FETCH_DONE: {
            const { listaStudenta } = action as FetchStudentsSuccess;
            const ids = [];
            
            return adapter.addMany(listaStudenta,state);
        }
        case DELETE_STUDENT:{
            console.log(state.entities, "STATEEEE");
         
            const {id} = action as deleteStudent;
            return adapter.removeOne(id, state);        
        }
        default: {

            return state;
        }
    }
} 

export const selectors = adapter.getSelectors();
