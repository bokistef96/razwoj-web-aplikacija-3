import {ActionReducerMap} from '@ngrx/store';
import { Student } from '../models/student';

import  studentsReducer, {StudentState} from './reducers/students.reducer';

export interface State {
    students: StudentState
   
}

export const rootReducer: ActionReducerMap<State> = {
    students: studentsReducer
    
}