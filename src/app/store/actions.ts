import {Action} from '@ngrx/store';
import { Student } from '../models/student';
import {getStudents} from '../service/api';
export const RETURN_STUDENTS = "RETURN_STUDENTS";
export const ADD_STUDENT = "ADD_STUDENT";
export const DELETE_STUDENT = "DELETE_STUDENT";
export const FETCH_STUDENTS = "FETCH_STUDENTS";
export const STUDENTS_FETCH_DONE = "STUDENTS_FETCH_DONE";




export class addStudent implements Action {
    type = ADD_STUDENT;
    constructor(public student:Student){
      
    }
}

export class FetchStudentsSuccess implements Action {
    type = STUDENTS_FETCH_DONE;
    listaStudenta: Student[]; 
    constructor(studenti: Student[] ) {
        this.listaStudenta = [...studenti]; 
    }
}

export class fetchStudents implements Action {
    type = FETCH_STUDENTS;
    constructor(){
    }
}

export class deleteStudent implements Action {
    type = DELETE_STUDENT;
    id:number;
    constructor(id:number){
        this.id=id;
      
    }
}
