const URL_ROOT = 'http://localhost:3005';

export function getStudents(){
    const request = fetch(`${URL_ROOT}/students`, { method: 'GET'})
                    .then(response => response.json());
    
    return request;
  }

  export function addNewStudent(obj){    
    fetch("http://localhost:3005/students", {
            method: 'post',
            headers: {
                
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
          })
   
    }

    export function deleteStudentA(id) {
        fetch(`http://localhost:3005/students/${id}`, {
              method: 'delete',
              headers: {
                  
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            })
            
      }
     
  
    