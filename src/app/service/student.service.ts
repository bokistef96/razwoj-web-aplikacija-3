import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Student } from '../models/student';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient ) {
  
  }

  public fetchStudents(): Observable<Student[]> {
    
    return this.http.get<Student[]>('http://localhost:3005/students');
  }
  
  public addStudent(student: Student) {
    this.http.post('http://localhost:3005/students', student)
              .subscribe(response => console.log(response)); 
          
  }



  public deleteStudent(id: Number) {
    this.http.delete(`http://localhost:3005/students/${id}`) 
              .subscribe(response => console.log(response));
  }
  
}
