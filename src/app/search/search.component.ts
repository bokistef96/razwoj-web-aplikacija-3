import { Component, OnInit } from '@angular/core';
import { State } from '../store'
import {Store} from '@ngrx/store';
import { Student } from '../models/student';
import { deleteStudent,fetchStudents } from '../store/actions';
import { Observable} from 'rxjs';
import { selectors } from '../store/reducers/students.reducer';
import { StudentsService } from '../service/student.service';




@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  students$:Observable<Student[]>;

  constructor(
    private store$: Store<State>,
    private StudentsService: StudentsService,

  ) { }

  ngOnInit() {
    this.store$.dispatch(new fetchStudents())
    this.students$ = this.store$.select(state => selectors.selectAll(state.students)); // pomocu ovog vracamo stanje iz store-a
    this.students$.forEach(student=>console.log(student));
  }

  onDeleteStudent(id:number){

    this.StudentsService.deleteStudent(id);
    this.store$.dispatch(new deleteStudent(id));
   
  }




}
