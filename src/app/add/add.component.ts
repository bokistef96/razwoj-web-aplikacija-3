import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../store';
import { Student } from '../models/student';
import { addStudent } from '../store/actions';
import { StudentsService } from '../service/student.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  student:Student = new Student("","","","",null,null);
  constructor(
    private store$: Store<State>,
    private studentsService: StudentsService
  ) 
  { }
  
  ngOnInit() {
  }

  onAddStudent(){
    this.studentsService.addStudent(this.student);

    this.store$.dispatch(new addStudent(this.student));
  }

}
