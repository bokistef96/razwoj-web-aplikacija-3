import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { AddComponent } from './add/add.component';



const routes: Routes=[
  { path: '', component: HomeComponent },
  { path: 'prikazi', component: SearchComponent },
  { path: 'dodaj', component: AddComponent },

]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
],
exports: [
    RouterModule
],
declarations: []
})
export class AppRoutingModule { }
export const routingComponents=[HomeComponent,SearchComponent,AddComponent]
