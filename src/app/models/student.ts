

export class Student {

    constructor(
        public ime: string,
        public prezime: string,
        public brIndexa: string,
        public brPolozenihIspita: string,
        public prosek: number,
        public id:number
    ) {

    }
}