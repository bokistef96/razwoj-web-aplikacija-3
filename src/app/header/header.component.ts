import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  elfak:string;

  constructor() {
    this.elfak = 'assets/images/elfak.jpg'
   }
  
  ngOnInit() {
  }

}
